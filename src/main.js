// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import MyPlugin from '../src/plugin'
import filters from './filters/filters'
import i18n from './i18n'
import Vuex from 'vuex'
import { LMap, LMarker, LTileLayer} from 'vue2-leaflet'
import 'leaflet/dist/leaflet.css';

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.use(Vuex)
Vue.use(VueMaterial)
Vue.use(MyPlugin)
Object.keys(filters).forEach(k => Vue.filter(k, filters[k]))

Vue.config.productionTip = false

const store = new Vuex.Store({
  state: {
    favouriteUsers: [],
    selectedUser: {}
  },
  getters: {
    selectedUser (state) {
      return state.selectedUser
    },
    favouriteUsers (state) {
      return state.favouriteUsers
    }
  },
  mutations: {
    addFavourite (state, user) {
      state.favouriteUsers.push(user);
    },
    deleteFavourite (state, index) {
      state.favouriteUsers.splice(index, 1);
    },
    setSelectedUser (state, user) {
      state.selectedUser = user;
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  i18n,
  router,
  template: '<App/>',
  components: { App }
})
