export default {
    gender: {
        male: 'Hombre',
        female: 'Mujer'
    },
    countries: {
        NO: 'Noruega',
        TR: 'Turquía',
        ES: 'España',
        FR: 'Francia',
        US: 'Estados Unidos',
        DE: 'Alemania',
        CH: 'República Checa',
        IR: 'Irán',
        DK: 'Dinamarca',
        NL: 'Países Bajos',
        AU: 'Australia',
        FI: 'Finlandia',
        CA: 'Canadá',
        IE: 'Irlanda',
        GB: 'Gran Bretaña',
        BR: 'Brasil',
        NZ: 'Nueva Zelanda'
    }
}