const axios = require('axios')
class ApiService {
    /**
     * Crea configuración base de la conexión
     * @return {*} Coniguración de la conexión
     */
    config(url) {
        const cfg = {
            baseURL: url,
            withCredentials: false
        }

        return cfg
    }

    /**
     * @override
     */
    constructor(url) {
        this.connection = axios.create(this.config(url))
    }
    /**
     * @override
     */
    getList(url, config = {}) {
        return this.connection.get(url, config)
    }

    /**
     * @override
     */
    get(url, config = {}) {
        return this.connection.get(url, config)
    }

    /**
     * @override
     */
    post(url, payload, config = {}) {
        return this.connection.post(url, payload, config);
    }

}

export default ApiService
