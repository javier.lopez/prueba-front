import ApiService from './api.service'

class FavouritesService extends ApiService {
  /**
   * constructor
   */
  constructor() {
    super('http://localhost:3000')
  }
  /**
   * Get companies list
   * @param {Object} sort Sort
   * @param {Object} config Axios config
   * @return {Object} Response with users
   */
  postList = (body, config) => {
    return this.post(`/favourites`, body, config)
  }

}

const favouritesService = new FavouritesService()

export default favouritesService
