import ApiService from './api.service'

class RandomUserService extends ApiService {
  /**
   * constructor
   */
  constructor() {
    super('https://randomuser.me/api')
  }
  /**
   * Get companies list
   * @param {Object} sort Sort
   * @param {Object} config Axios config
   * @return {Object} Response with users
   */
  getRandomUsers = (config) => {
    return this.getList(`/?results=100`, config)
  }

}

const randomUserService = new RandomUserService()

export default randomUserService
