import randomUserService from './services/randomuser.service'
import favouritesService from './services/favourites.service'

const MyPlugin = {
    /**
     * Installs plugin on vue project.
     * @param {Object} Vue Vue Object
     * @param {Object} Config Configuration of the plugin
     */
    install(Vue) {
        Vue.prototype.$randomUserService = randomUserService
        Vue.prototype.$favouritesService = favouritesService
    }
}

export default MyPlugin