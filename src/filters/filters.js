const getLocale = () => {
    return 'es-ES';
};

const filters = {
    date(dateValue) {
        return new Date(dateValue).toLocaleDateString(getLocale());
    }
};

export default filters;
