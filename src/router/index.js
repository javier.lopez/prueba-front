import Vue from 'vue'
import Router from 'vue-router'
import Vista1 from '@/views/Vista1'
import Vista2 from '@/views/Vista2'
import Vista3 from '@/views/Vista3'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Vista1',
      component: Vista1
    },
    {
      path: '/vistaUsuario',
      name: 'Vista2',
      component: Vista2
    },
    {
      path: '/vistaFavoritos',
      name: 'Vista3',
      component: Vista3
    }
  ]
})
